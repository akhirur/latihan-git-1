
// Soal 1

function range(startNum, finishNum) {
  var num = [];

  if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      num.push(i)
    }
// contoh penggunaan sort
//     for (var i = finishNum; i <= startNum; i++) {
//      num.push(i)
//     }
//     function compareNumbers(a, b) {
//      return b - a;
//     }
//     num.sort(compareNumbers);
  } else if (startNum == null || finishNum == null) {
    num = -1;
  } else {
    for (var i = startNum; i <= finishNum; i++) {
      num.push(i)
    }
  }
  return num;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

// Soal 2

function rangeWithStep(startNum2, finishNum2, step) {
  var num2 = [];
  var j = startNum2;
  if (startNum2 > finishNum2) {
    while (j >= finishNum2) {
      num2.push(j);
      j -= step;
    }
  }else if (startNum2 < finishNum2) {
    while (j <= finishNum2) {
      num2.push(j);
      j += step;
    }
  }
return num2;
}


console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// Soal 3

function sum(angkaAwal, angkaAkhir, selisih) {
  var arr = [];
  var x = angkaAwal;
  var total = 0;
  var selisihnya = selisih;

  if (selisihnya == null) {
    selisihnya = 1;
  }

  if (angkaAwal > angkaAkhir) {
    while (x >= angkaAkhir) {
      arr.push(x);
      x -= selisihnya;
    }
  } else if (angkaAwal < angkaAkhir) {
    while (x <= angkaAkhir) {
      arr.push(x);
      x += selisihnya;
    }
  } else {
    if (angkaAwal == null) {
      x = angkaAkhir;
      arr.push(x);
    } else if (angkaAkhir == null) {
      x = angkaAwal;
      arr.push(x);
    }
  }

  if (arr != 0) {
    for(var x2 = 0; x2 <arr.length; x2++){
      total += arr[x2];
    }
    } else {
      total = 0;
    }
  return total;
}


console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// Soal 4

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

function dataHandling() {
  for (var i = 0; i < input.length; i++) {
    // console.log(input[i] + "\n");
    console.log("Nomor ID :" + input[i][0]);
    console.log("Nama Lengkap :" + input[i][1]);
    console.log("TTL :" + input[i][2] + input[i][3]);
    console.log("Hobi :" + input[i][4]);
  }
}

dataHandling();

// Soal 5

function balikKata(kata) {
  var katanya = "";
  var j = kata.length;
    for (var i = j - 1; i >= 0; i--) {
      katanya += kata[i]
    }
  return kata;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal 56

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input){
    var data = input;
    var nama = input[1] + 'Elsharawy';
    var prov = 'Provinsi ' + input[2];
    var jk = 'Pria';
    var sch = 'SMA Internasional Metro';

    data.splice(1,1, nama);
    data.splice(2,1, prov);
    data.splice(4,1, jk, sch);

    console.log(data); // array baru

    var date = input[3];
    var newDate = date.split("/");
    var month = '';

    if (newDate[1] >= 1 && newDate[1] <= 12) {
        switch (newDate[1]) {
            case '01': {
                month = 'Januari';
                break;
            }
            case '02': {
                month = 'Februari';
                break;
            }
            case '03': {
                month = 'Maret';
                break;
            }
            case '04': {
                month = 'April';
                break;
            }
            case '05': {
                month = 'Mei';
                break;
            }
            case '06': {
                month = 'Juni';
                break;
            }
            case '07': {
                month = 'Juli';
                break;
            }
            case '08': {
                month = 'Agustus';
                break;
            }
            case '09': {
                month = 'September';
                break;
            }
            case '10': {
                month = 'Oktober';
                break;
            }
            case '11': {
                month = 'November';
                break;
            }
            case '12': {
                month = 'Desember';
                break;
            }
        }
    }
    console.log(month);

    var joinDate = newDate.join('-');
    console.log(joinDate);

    var sortDate = newDate.sort(function (value1, value2) { return value2 - value1 } )
    console.log(sortDate);

    var sliceName = nama.slice(0, 15);
    console.log(sliceName);

}
dataHandling2(input);
