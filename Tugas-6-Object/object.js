var now = new Date()
var thisYear = now.getFullYear() // 2021 (tahun sekarang)

function arrayToObject(arr) {
  for (var i = 0; i < arr.length;) {
      var orang = {
        firstName : arr[i][0],
        lastName : arr[i][1],
        gender : arr[i][2],
        age : arr[i][3]
      }
      if (!orang.age || orang.age > thisYear) {
        orang.age = "Invalid Birth Year"
      } else {
        orang.age = thisYear - orang.age
      }
      i++
      process.stdout.write(i +". "+ orang.firstName + " " + orang.lastName + ": ")
      console.log(orang)
  }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
/*
    1. Bruce Banner: {
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: {
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
/*
    1. Tony Stark: {
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: {
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]) // ""


// Soal 2

function shoppingTime(memberId, money) {
    var info = "";
    var barangSale = [{
      namaBarang:"Sepatu Stacattu", harga:1500000},
      {namaBarang:"Baju Zoro", harga:500000},
      {namaBarang:"Baju H&N", harga: 250000},
      {namaBarang:"Sweater Uniklooh", harga: 175000},
      {namaBarang:"Casing Handphone", harga: 50000}]

    var member = {
      memberId : memberId,
      money : money,
      listPurchased : [],
      changeMoney : 0
    }

    function compareNumbers(a, b) {
    return a.harga > b.harga;
    }
    barangSale.sort(compareNumbers);

    if (memberId == null || memberId == "") {
      info = "Mohon maaf, toko X hanya berlaku untuk member saja";
      return info;
    } else if (money < 50000) {
        info = "Mohon maaf, uang tidak cukup";
        return info;
      } else {
        for (var i = 0; i < barangSale.length;i++) {
          if (money >= barangSale[i].harga) {
            money = money - barangSale[i].harga;
            member.listPurchased.push(barangSale[i].namaBarang);
          }
        }
        member.changeMoney = money;
      }
    return member;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3

function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var data = [];
  if (arrPenumpang == null || arrPenumpang ==""){
    return [];
  } else {
    arrPenumpang.forEach((item, i) => {
      var asal = rute.indexOf(item[1])
      var sampai = rute.indexOf(item[2])
      var panjangRute = parseInt(sampai - asal)

      data.push({
        penumpang : item[0],
        naikDari : item[1],
        tujuan : item[2],
        bayar : panjangRute*2000
      })
    });
  }
  return data;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
