let golden = () => console.log("this is golden!!")

golden()


let newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function() {console.log(`${firstName} ${lastName}`)}}
}
//Driver Code
newFunction("William", "Imoh").fullName()


const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)


let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
//Driver Code

let combined = [...west, ...east]

console.log(combined)


const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,
    consectetur adipiscing elit, ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before)
