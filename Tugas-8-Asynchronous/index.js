// di index.js
var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
var adaWaktu = 10000;
function compareNumbers(a, b) {
return a.timeSpent - b.timeSpent;
}
books.sort(compareNumbers);

var buku = books.length;

function execute(adaWaktu, index, buku) {
    readBooks(adaWaktu, books[index], function(remainingTime) {
        adaWaktu = remainingTime;
        buku = buku - 1;
        if (buku > 0) {
            execute(adaWaktu, index+1, buku);
        }
    });
}

execute(adaWaktu, 0, buku);
