console.log("\n"+"Soal 1"+"\n");

console.log("LOOPING PERTAMA");
var angka1 = 0;
var loop = 0;
while (loop < 20) {
    if (loop < 10) {
      angka1 += 2;
      console.log(angka1 + " - I love coding");
    } else {
          if (angka1 == 20) {
            console.log("LOOPING KEDUA");
          }
          console.log(angka1 + " - I will become a mobile developer");
          angka1 -= 2;
    }
    loop++;
}


console.log("\n"+"Soal 2"+"\n");

for (var angka = 1; angka <= 20; angka++) {
  if (angka%3 == 0 && angka%2 != 0) {
    console.log(angka + "- I Love Coding");
  } else if (angka%2 == 0) {
    console.log(angka + "- Berkualitas");
  } else if (angka%2 != 0) {
    console.log(angka + "- Santai");
  }
}
//
//
console.log("\n"+"Soal 3"+"\n");

var hasil = "";
    for (var i = 1; i < 5; i++) {
        for (var j = 1; j < 9; j++) {
            hasil += "#";
    }
    hasil += "\n";
}
console.log(hasil);
//
//
console.log("\n"+"Soal 4"+"\n");

var hasil2 = "";
for (var a = 1; a <=7; a++) {
    for (var b = 1; b <= a; b++) {
      hasil2 += "#";
    }
    hasil2 += "\n";
}
console.log(hasil2);
//
//
console.log("\n"+"Soal 5"+"\n");

var hasil3 = "";
var kotak = 8;
for (var y = 0; y < kotak; y++) {
  for (var x = 0; x < kotak; x++) {
    if ((y + x)%2 == 0) {
      hasil3 += " ";
    } else {
      hasil3 += "#";
    }
  }
  hasil3 += "\n";
}
console.log(hasil3);
